<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="dcterms.date" content="2019-11-01" />
  <meta name="description" content="Breaking down the improvements to both speed and performance made to my ET-Jekyll theme step-by-step" />
  <title>Improving Tufte CSS for Jekyll</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="../style.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<nav>
    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about.html">About</a></li>
        <li><a href="/blog">Blog</a></li>
        <li><a href="/uses.html">Uses</a></li>
        <li><a href="/feeds/posts.xml">RSS</a></li>
    </ul>
</nav>
<article>
<header id="title-block-header">
<h1 class="title">Improving Tufte CSS for Jekyll</h1>
<p class="date">November 01, 2019</p>
</header>
<nav id="TOC" role="doc-toc">
<ul>
<li><a href="#introductions" id="toc-introductions">Introductions</a></li>
<li><a href="#minor-fixes-one-year-later" id="toc-minor-fixes-one-year-later">Minor Fixes One Year Later</a></li>
<li><a href="#first-contentful-paint-input-delay" id="toc-first-contentful-paint-input-delay">First Contentful Paint &amp; Input Delay</a></li>
<li><a href="#fixing-render-blocking-items" id="toc-fixing-render-blocking-items">Fixing Render Blocking Items</a></li>
<li><a href="#lighthouse-numbers" id="toc-lighthouse-numbers">Lighthouse Numbers</a></li>
<li><a href="#final-thoughts" id="toc-final-thoughts">Final Thoughts</a></li>
</ul>
</nav>
<p><em>After creating the ET-Jekyll theme almost two years ago</em>, I finally got around to revamping the structure and improving a lot of minor performance issues. Items that have been surely needing of updates for the last couple of years.</p>
<h2 id="introductions">Introductions</h2>
<p>I&#x2019;ve always been a sucker for Edward Tufte&#x2019;s incredibly simple, yet powerful design work used in his books and handout projects. So, in 2018 I released a Tufte CSS inspired Jekyll theme for the open source community. I called it <a href="https://et-jekyll.netlify.com">ET-Jekyll</a> (so original, I know). Tufte CSS was a great starting point for my Jekyll theme, but there were areas I thought could use some minor improvements.</p>
<p>Feel free to read all the details on the design here: <a href="https://et-jekyll.netlify.com/et-jekyll-theme/">ET-Jekyll theme details</a></p>
<h2 id="minor-fixes-one-year-later">Minor Fixes One Year Later</h2>
<p>When I finally circled back to this project recently, I noticed some minor issues that could be improved right away with little to no effort. Let&#x2019;s see the changes made at a glance:</p>
<ul>
<li>Sidenote, marginnote and figure element restyling (flexbox)</li>
<li>Table styling fixes (right alignment issues)</li>
<li>Switch MathJax over to SVG embeds (performance fixes)</li>
<li>Simplify HTML skeleton of main pages, remove overkill classes</li>
<li>Remove lazysizes.js to save on load times</li>
<li>Fallback on font-display for font loading</li>
<li>Inline all CSS for faster initial paint</li>
<li>Minor link :hover coloring (accessibility)</li>
<li>Add missing image link on example page</li>
<li>Update details post structure</li>
</ul>
<p>You can view all the updates in more detail <a href="https://github.com/bradleytaunt/ET-Jekyll/commit/254f9e8f28764c9525ba7405bbbfa18a3867d241">here</a>.</p>
<p><strong>So what did this accomplish?</strong> Let&#x2019;s break it down below.</p>
<h2 id="first-contentful-paint-input-delay">First Contentful Paint &amp; Input Delay</h2>
<p>The new improvements have netted the theme a savings of 300ms on first paint and reduced the input delay by 150ms. Small wins - but wins nonetheless since every millisecond counts.</p>
<figure>
<img src="/public/images/tufte-first-paint.webp" alt="First paint comparison" />
<figcaption>
First contentful paint savings: 300ms (<a href="/public/images/tufte-first-paint.webp">direct link to image)</a>
</figcaption>
</figure>
<figure>
<img src="/public/images/tufte-input-delay.webp" alt="Input delay comparison" />
<figcaption>
Reduction in input delay: 370ms down to 220ms (<a href="/public/images/tufte-input-delay.webp">direct link to image</a>)
</figcaption>
</figure>
<h2 id="fixing-render-blocking-items">Fixing Render Blocking Items</h2>
<p>The original theme reported a few items that were slowing down the initial render for the end-users:</p>
<table>
<thead>
<tr class="header">
<th>URL</th>
<th>Size (KB)</th>
<th>Savings (ms)</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>/css/style.css</td>
<td>2.0</td>
<td>150</td>
</tr>
<tr class="even">
<td>/lazysizes@4.0.1/lazysizes.js</td>
<td>5.0</td>
<td>960</td>
</tr>
<tr class="odd">
<td>MathJax.js?config=TeX-MML-AM_CHTML</td>
<td>18.0</td>
<td>1,260</td>
</tr>
</tbody>
</table>
<p>These items were resolved by:</p>
<ul>
<li>Rendering all styling inline (therefore removing the extra HTTP request)</li>
<li>Removing the lazysizes library altogether (browsers plan to support lazy loading natively in the future)
<ul>
<li>The future plan is to integrate something like Cloudinary for out-of-the-box image processing</li>
</ul></li>
<li>Switch over MathJax to render equations as embedded SVGs (saves on bandwidth rendering an entire extra typeface)</li>
</ul>
<h2 id="lighthouse-numbers">Lighthouse Numbers</h2>
<p>Though it might not look like much, the updated theme receives a 4-point boost to its performance rating during a Lighthouse audit. Having a perfect score would be even better, but I can settle for 2-points under (for now).</p>
<h4 id="old-version">Old Version</h4>
<table>
<thead>
<tr class="header">
<th>Performance</th>
<th>Accessibility</th>
<th>Best Practices</th>
<th>SEO</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>94</td>
<td>100</td>
<td>100</td>
<td>100</td>
</tr>
</tbody>
</table>
<h4 id="new-version">New Version</h4>
<table>
<thead>
<tr class="header">
<th>Performance</th>
<th>Accessibility</th>
<th>Best Practices</th>
<th>SEO</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>98</td>
<td>100</td>
<td>100</td>
<td>100</td>
</tr>
</tbody>
</table>
<h2 id="final-thoughts">Final Thoughts</h2>
<p>This project could still use some more fine-tuning, but for now I&#x2019;m fairly happy with the outcome. Even the smallest boost in performance and rendering time makes me feel like I accomplished something worthwhile.</p>
<p>Please don&#x2019;t hesitate to suggest features or point out any issues you happen to stumble across if you plan to use ET-Jekyll. Thanks for reading!</p>
<div class="tip">
    <h3>Found this article helpful?</h3>
    <p>Please consider donating to help pay for hosting costs and support the creation of new articles, demos, tutorials, etc. I greatly appreciate it.</p>
    <a href="/donate.html" class="button">Support</a>
</div>
</article>
<footer>
    <p>Powered by <a target="_blank" href="https://pblog.xyz">pblog</a><br>
    Made with &hearts; for a simpler web<br>
    <a href="/donate.html">Support this website</a></p>
</footer>
<script data-goatcounter="https://tdarb.goatcounter.com/count"
        async src="//gc.zgo.at/count.js"></script>
</body>
</html>
