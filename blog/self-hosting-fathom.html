<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="dcterms.date" content="2021-02-02" />
  <title>Self-Hosting Fathom Analytics with DigitalOcean</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="../style.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<nav>
    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about.html">About</a></li>
        <li><a href="/blog">Blog</a></li>
        <li><a href="/uses.html">Uses</a></li>
        <li><a href="/feeds/posts.xml">RSS</a></li>
    </ul>
</nav>
<article>
<header id="title-block-header">
<h1 class="title">Self-Hosting Fathom Analytics with DigitalOcean</h1>
<p class="date">February 02, 2021</p>
</header>
<nav id="TOC" role="doc-toc">
<ul>
<li><a href="#prerequisites" id="toc-prerequisites">Prerequisites</a></li>
<li><a href="#create-a-droplet" id="toc-create-a-droplet">Create a Droplet</a></li>
<li><a href="#enter-the-matrix-not-really" id="toc-enter-the-matrix-not-really">Enter the Matrix (not really)</a></li>
<li><a href="#domains" id="toc-domains">Domains</a></li>
<li><a href="#ssl-ftw" id="toc-ssl-ftw">SSL FTW</a></li>
<li><a href="#the-final-lap" id="toc-the-final-lap">The Final Lap</a></li>
</ul>
</nav>
<p>Since my previous post walked through the process of setting up <a href="/fathom-analytics-netlify">Fathom PRO on Netlify</a>, I figured it made sense to create a similar tutorial for the &#x201C;Lite&#x201D; variation, self-hosted on DigitalOcean.</p>
<p>Please note that while I think the PRO version of <a target="_blank" href="https://usefathom.com/ref/DKHJVX">Fathom Analytics</a> is truly great, for my small, niche blog it seemed overkill compared to self-hosting. Switching over from $14/mo to $5/mo while retaining most of the same functionality was a no-brainer. Choose the option that best suits your needs (or in the case - budget &amp; bandwidth).</p>
<p>With that cleared up - let&#x2019;s get into it!</p>
<h2 id="prerequisites">Prerequisites</h2>
<ol type="1">
<li>One or more website(s) where you would like to include analytics</li>
<li><a target="_blank" href="https://m.do.co/c/74b3fd11c07a">DigitalOcean account</a> (<strong>this link will give you a $100 credit!</strong>)</li>
<li>Positive attitude and passion for privacy-respecting analytics!</li>
</ol>
<h2 id="create-a-droplet">Create a Droplet</h2>
<p>Once your DigitalOcean account is setup, navigate to the <a target="_blank" href="https://marketplace.digitalocean.com">Marketplace</a> and search for <code>Fathom Analytics</code>. Then click the <code>Create Fathom Analytics Droplet</code>.</p>
<p>From here you&#x2019;ll be brought to a page that allows you to customize the specifications of your new droplet. If you&#x2019;re a smaller traffic site (personal blog, etc) selecting the <strong>$5/month</strong> Basic Shared CPU option is your best bet.</p>
<figure>
<img src="/public/images/fathom-create-droplet-details.webp" alt="Fathom Droplet Details">
<figcaption>
Creating the new droplet (<a href="/public/images/fathom-create-droplet-details.webp">direct link to image</a>)
</figcaption>
</figure>
<p>Select the data-center region based on where most of your traffic originates from. I would suggest enabling <code>IPv6</code> and setting up your authentication via SSH instead of a regular password system. Adding backups is entirely at your own discretion.</p>
<p>Once you&#x2019;re ready, click <strong>Create Droplet</strong>.</p>
<h2 id="enter-the-matrix-not-really">Enter the Matrix (not really)</h2>
<p>Once DigitalOcean finishes spinning up your new droplet, open a terminal and connect to it by entering:</p>
<pre class="shell"><code>ssh root@YOUR_DROPLET_IP</code></pre>
<p>If you setup your login via SSH everything should work as-is. If you went the password route, you&#x2019;ll given a prompt to enter it.</p>
<p>Now that you&#x2019;re connected, Fathom will guide you through a simple configuration setup. It&#x2019;s fairly straightforward and painless. Once complete, move to the next step.</p>
<h2 id="domains">Domains</h2>
<p>You&#x2019;ll most likely want to host this instance on your own domain or subdomain - instead of connecting directly via the droplet&#x2019;s <code>IP</code>. Head over to your <strong>Networking</strong> page in the sidebar of DigitalOcean and add your custom domain.</p>
<p>Then, click on that newly added domain - we need to add some new records. You&#x2019;re going to add two new <code>A</code> records to this domain:</p>
<table>
<thead>
<tr>
<th>
Type
</th>
<th>
Hostname
</th>
<th>
Value
</th>
</tr>
</thead>
<tbody>
<tr>
<td>
A
</td>
<td>
@
</td>
<td>
YOUR_DROPLET_IP
</td>
</tr>
<tr>
<td>
A
</td>
<td>
www
</td>
<td>
YOUR_DROPLET_IP
</td>
</tr>
</tbody>
</table>
<p>The last thing you need to do is set your nameservers to point to DigitalOcean:</p>
<pre class="shell"><code>ns1.digitalocean.com
ns2.digitalocean.com
ns3.digitalocean.com</code></pre>
<p>Give it some time to propagate and you&#x2019;ll be in business!</p>
<h2 id="ssl-ftw">SSL FTW</h2>
<p>There is hardly a good reason not to practice security on the web, so setting up your new analytics to be served over <code>HTTPS</code> is just the smart thing to do. Did I mention that this is completely free as well? See - no excuses.</p>
<p>In order to get a free SSL certificate setup, you&#x2019;ll need to install <code>certbot</code>. While connected to your droplet, enter the following:</p>
<pre class="shell"><code>sudo apt-get install python-certbot-nginx</code></pre>
<p>Once installed, enter the following to setup SSL (remember to swap out the domain with your own):</p>
<pre class="shell"><code>certbot --nginx -d your-cool-domain.com</code></pre>
<p>Follow the steps (it&#x2019;s very quick and easy) and you&#x2019;ll have <code>HTTPS</code> setup in a jiffy!</p>
<h2 id="the-final-lap">The Final Lap</h2>
<p>The last thing to do is login to your newly self-hosted Fathom instance, add your site you wish to track, grab the generated tracking code and then slap that badboy on whatever pages you need to track!</p>
<p>Congrats! You&#x2019;re now officially running your own set of analytics tools. You should be happy about what you&#x2019;ve accomplished and proud for respecting your users&#x2019; privacy!</p>
<div class="tip">
    <h3>Found this article helpful?</h3>
    <p>Please consider donating to help pay for hosting costs and support the creation of new articles, demos, tutorials, etc. I greatly appreciate it.</p>
    <a href="/donate.html" class="button">Support</a>
</div>
</article>
<footer>
    <p>Powered by <a target="_blank" href="https://pblog.xyz">pblog</a><br>
    Made with &hearts; for a simpler web<br>
    <a href="/donate.html">Support this website</a></p>
</footer>
<script data-goatcounter="https://tdarb.goatcounter.com/count"
        async src="//gc.zgo.at/count.js"></script>
</body>
</html>
