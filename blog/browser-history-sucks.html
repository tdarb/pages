<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="dcterms.date" content="2019-04-20" />
  <meta name="description" content="Redesigning the default browser history interface for better UX" />
  <title>Browser History Sucks</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="../style.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<nav>
    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about.html">About</a></li>
        <li><a href="/blog">Blog</a></li>
        <li><a href="/uses.html">Uses</a></li>
        <li><a href="/feeds/posts.xml">RSS</a></li>
    </ul>
</nav>
<article>
<header id="title-block-header">
<h1 class="title">Browser History Sucks</h1>
<p class="date">April 20, 2019</p>
</header>
<nav id="TOC" role="doc-toc">
<ul>
<li><a href="#what-browsers-get-wrong" id="toc-what-browsers-get-wrong">What browsers get wrong</a></li>
<li><a href="#why-not-use-extensions" id="toc-why-not-use-extensions">Why not use extensions?</a></li>
<li><a href="#two-subtle-improvements" id="toc-two-subtle-improvements">Two subtle improvements</a>
<ul>
<li><a href="#session-length" id="toc-session-length">Session length</a></li>
<li><a href="#return-visits" id="toc-return-visits">Return visits</a></li>
<li><a href="#last-restored-tabs" id="toc-last-restored-tabs">Last restored tabs</a></li>
</ul></li>
<li><a href="#far-from-perfect" id="toc-far-from-perfect">Far from perfect</a></li>
</ul>
</nav>
<p><em>Have you ever needed to step back through your browser history</em> to find a particular site or product? Do you remember that experience being good? Most likely not.</p>
<p>Much like printers, the design of browser history interfaces hasn&#x2019;t changed in years. This would be fine if these UIs had been well thought out and optimized for an easy user experience - but they weren&#x2019;t.</p>
<p>Browser history views rely on the user&#x2019;s own memory for more in-depth searches. This defeats the whole purpose of having a robust, documented history. The browser should be doing this heavy-lifting.</p>
<h2 id="what-browsers-get-wrong">What browsers get wrong</h2>
<p>Modern browsers give the general public too much credit when it comes to memory (I don&#x2019;t mean this as an insult!). To assume users remember the URL or site name when browsing random pages is short-sighted. I find myself asking these types of questions when jumping back into my view history far too often:</p>
<ul>
<li><p>&#x201C;That article had <em>something</em> to do with CSS&#x2026;&#x201D;</p></li>
<li><p>&#x201C;I remember seeing a beautifully designed site a month ago but have no clue what the URL was&#x2026;&#x201D;</p></li>
<li><p>&#x201C;My browser crashed and I can&#x2019;t recall that [example website] I had pinned in my tab for weeks&#x2026;&#x201D;</p></li>
</ul>
<p>For reference, let&#x2019;s take a look at the current Chrome (73) history view:</p>
<figure>
<img src="/public/images/browser-history-01.webp" alt="Default Chrome History" />
<figcaption aria-hidden="true">Default Chrome History</figcaption>
</figure>
<p>As you may have noticed - this UI is lackluster at best. An oversimplified search field in the header is the only means of filtering items.</p>
<h2 id="why-not-use-extensions">Why not use extensions?</h2>
<p>I know using browser extensions or tagging favorites can alleviate some of these issues. This is great, but why not simplify everything by having these features <em>inside</em> the history view? If an extension can add these features, why not have those extras built-in?</p>
<h2 id="two-subtle-improvements">Two subtle improvements</h2>
<p>A little goes a long way. With just two small changes, we can drastically increase the history view&#x2019;s UX.</p>
<p>We start by adding <u>a date picker</u>. Users open the new calendar icon to filter by days, months or years before searching. Seems trivial, but this saves the headache of filtering through all saved history.</p>
<figure>
<img src="/public/images/browser-history-02.webp" alt="Chrome History with date picker" />
<figcaption aria-hidden="true">Chrome History with date picker</figcaption>
</figure>
<p>The second small functional change we can make is including extra subcategories. These new options allow users to focus their searches based on:</p>
<ul>
<li>Session length</li>
<li>Number of return visits</li>
<li>Last restored tabs</li>
</ul>
<h3 id="session-length">Session length</h3>
<figure>
<img src="/public/images/browser-history-03.webp" alt="Chrome History by session length" />
<figcaption aria-hidden="true">Chrome History by session length</figcaption>
</figure>
<p>Allow users to display their history filtered by session duration. This helps when searching for an stagnant page or pinned site during a user&#x2019;s long session. An example default would allow filtering by:</p>
<ul>
<li>longest to shortest</li>
<li>shortest to longest</li>
<li>pinned tabs</li>
</ul>
<h3 id="return-visits">Return visits</h3>
<figure>
<img src="/public/images/browser-history-04.webp" alt="Chrome History by return visits" />
<figcaption aria-hidden="true">Chrome History by return visits</figcaption>
</figure>
<p>When users make repeat visits to a site or web app, the browser should keep a record of return sessions. This allows the user to refine their search by many or singular visits.</p>
<h3 id="last-restored-tabs">Last restored tabs</h3>
<figure>
<img src="/public/images/browser-history-05.webp" alt="Chrome History by restored tabs" />
<figcaption aria-hidden="true">Chrome History by restored tabs</figcaption>
</figure>
<p>A basic concept, but the ability for users to view all previous instances of restored tabs is helpful. This would fix most edge cases not covered by the other two categories.</p>
<h2 id="far-from-perfect">Far from perfect</h2>
<p>The Chrome (or any browser for that matter) browser history view is simplistic to a fault. The current UI is prone to human error, since it makes assumptions and relies heavily on user memory.</p>
<p>These are simple fixes that attempt to boost the basic UX of the history view. Are these concepts absolutely perfect? Not at all. Is it at least an improvement? I believe it is. When products decrease the effort required of it&#x2019;s users, I see that as a positive.</p>
<div class="tip">
    <h3>Found this article helpful?</h3>
    <p>Please consider donating to help pay for hosting costs and support the creation of new articles, demos, tutorials, etc. I greatly appreciate it.</p>
    <a href="/donate.html" class="button">Support</a>
</div>
</article>
<footer>
    <p>Powered by <a target="_blank" href="https://pblog.xyz">pblog</a><br>
    Made with &hearts; for a simpler web<br>
    <a href="/donate.html">Support this website</a></p>
</footer>
<script data-goatcounter="https://tdarb.goatcounter.com/count"
        async src="//gc.zgo.at/count.js"></script>
</body>
</html>
