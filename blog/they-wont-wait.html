<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="dcterms.date" content="2019-06-25" />
  <meta name="description" content="Your website is probably slow - learn how you can make it faster" />
  <title>They Won&#x2019;t Wait: A Warning for Slow Websites</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="../style.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<nav>
    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about.html">About</a></li>
        <li><a href="/blog">Blog</a></li>
        <li><a href="/uses.html">Uses</a></li>
        <li><a href="/feeds/posts.xml">RSS</a></li>
    </ul>
</nav>
<article>
<header id="title-block-header">
<h1 class="title">They Won&#x2019;t Wait: A Warning for Slow Websites</h1>
<p class="date">June 25, 2019</p>
</header>
<nav id="TOC" role="doc-toc">
<ul>
<li><a href="#we-can-do-better" id="toc-we-can-do-better">We can do better</a></li>
<li><a href="#data-doesnt-lie" id="toc-data-doesnt-lie">Data doesn&#x2019;t lie</a></li>
<li><a href="#small-and-easy-wins" id="toc-small-and-easy-wins">Small and easy wins</a></li>
<li><a href="#no-need-to-be-extreme" id="toc-no-need-to-be-extreme">No need to be extreme</a></li>
</ul>
</nav>
<p><em>Your website is probably slow</em>. I&#x2019;m not trying to make you feel bad or dismiss all the hard work you&#x2019;ve put into your project. Heck, performance might have been a core value of the design. But websites can always be faster.</p>
<p>People have become increasingly more impatient over the last decade when it comes to technology, specifically non-native web-based interactions. Users expect your website to load almost instantly or they will leave and try another site, probably one of your competitors. Why should they stick around if your competitors&#x2019; websites load half a second faster?</p>
<p>Users are tired of being bombarded with tracking scripts, having to download massive component libraries, forced to deal with &#x201C;accept cookies&#x201D; prompts, playing a small mini-game of &#x201C;close those ads!&#x201D;, and then being subjected to never-ending loading screens. This is not the internet we were promised.</p>
<blockquote>
<p>It&#x2019;s in my nature, I always liked <strong>speed</strong>.</p>
<p><cite>- Guy Lafleur<cite></p>
</blockquote>
<h2 id="we-can-do-better">We can do better</h2>
<p>If there is only one thing that you learn from this post, hopefully it&#x2019;s knowing to better value the <strong>time and money of your users</strong>. It&#x2019;s a user&#x2019;s <em>choice</em> to visit your website, so taking advantage of their time is extremely careless. Don&#x2019;t be arrogant and ignore the cost of data on most mobile plans either. Eating up a chunk of someone&#x2019;s data just for hitting your website is rage-inducing. That&#x2019;s how you can lose customers permanently.</p>
<p>Let&#x2019;s do an analogy, because <strong>I love stupid analogies</strong>:</p>
<p>Imagine going to your local hardware store because you need to buy a new hammer. Once you get to the entrance a woman holds the the door closed and asks you if it&#x2019;s alright for someone to follow you around the store today. You say no. She then follows up by asking if you accept their hardware store agreement before proceeding inside - you tell her &#x201C;sure&#x201D;. She finally opens the door and lets you in. As you walk into the store she quickly stuffs a few advertisements for other local businesses into you hand. &#x201C;Thanks&#x201D;, you mutter.</p>
<p>Once inside you realize the hardware store is <em>very big</em> and manually looking for a hammer might take a while. You walk up to the front desk to ask where you can find a hammer but notice the cashier is playing with their phone behind the counter. You try to get their attention but they simply raise their hand and shout &#x201C;Be with you in a minute&#x201D;. After a short while they get off their phone and <em>finally</em> listen to your question. They then tell you where to find the hammers.</p>
<p>Does this sound like a <em>fast</em> and easy experience?</p>
<p>As silly as this hypothetical trip to the hardware store might be, it&#x2019;s exactly what many current websites are putting their users through. Users - read <em>customers</em> - are coming to your website with a specific goal in mind; checking out a product, consuming information or just satisfying their curiosity. Stop putting so many blockers and excessive bloat in front of them.</p>
<h2 id="data-doesnt-lie">Data doesn&#x2019;t lie</h2>
<p>If my terrible analogy wasn&#x2019;t enough to convince you to implement better performance on your website, then maybe some &#x201C;BIG DATA&#x201D; will.</p>
<ul>
<li><a href="https://web.archive.org/web/20081117195303if_/http://home.blarg.net/~glinden/StanfordDataMining.2006-11-29.ppt">Amazon (PowerPoint, slide #15)</a>: 100 ms of latency resulted in 1% less sales.</li>
<li><a href="https://youtu.be/6x0cAzQ7PVs?t=936">Google (video)</a>: 500 ms caused a 20% drop in traffic.</li>
<li><a href="https://www.slideshare.net/devonauerswald/walmart-pagespeedslide">Walmart (slide #46)</a>: a 100 ms improvement brought up to 1% incremental revenue</li>
<li><a href="https://blog.mozilla.org/metrics/2010/04/05/firefox-page-load-speed-%E2%80%93-part-ii/">Mozilla</a>: Shaving 2.2 seconds off page load time increased downloads by 15.4%</li>
<li><a href="https://www.slideshare.net/stubbornella/designing-fast-websites-presentation/23-1_Create_a_component_library">Yahoo</a>: 400 ms resulted in a 5 to 9% drop in traffic</li>
</ul>
<p><small>All data taken from <a href="https://instant.page">instant.page</a> (which I am a huge fan of &#x2665;)</small></p>
<p>The fact something as small as 100 ms can have such a profound impact on your bottom-line should be eye-opening. You&#x2019;re leaving money of the table by not tackling even the low-hanging, easy performance wins. You need to start valuing your users&#x2019; time and stop serving them excessive garbage they never asked for.</p>
<h2 id="small-and-easy-wins">Small and easy wins</h2>
<p>Not all of these suggestions can work for every project (due to restrictions, brand guidelines, required marketing targets, etc.) but for most developers/designers they should be easy to implement: (in no particular order of importance)</p>
<ol type="1">
<li>Reduce the number of web requests
<ul>
<li><a href="https://developers.google.com/web/fundamentals/performance/get-started/httprequests-5">HTTP Requests</a></li>
</ul></li>
<li>Use web-safe fonts when available or if using custom fonts utilize the <code>font-display</code> property
<ul>
<li><a href="https://www.cssfontstack.com/">CSS Font Stack</a></li>
<li><a href="https://css-tricks.com/font-display-masses/">Font Display for the Masses</a></li>
</ul></li>
<li>Make proper use of <em>critical CSS</em>
<ul>
<li><a href="https://alexwright.net/web-design-secrets/how-to-use-critical-css/">How to Use Critical CSS</a></li>
<li>Automatically generate CSS based on &#x201C;above the fold&#x201D;: <a href="https://github.com/filamentgroup/criticalCSS">criticalCSS</a></li>
</ul></li>
<li>Process all media (images / videos) through 3rd party tools
<ul>
<li><a href="https://cloudinary.com/">Cloudinary</a></li>
<li><a href="https://kraken.io/">Kraken.io</a></li>
<li><a href="https://piio.co/">Piio</a></li>
<li>Sidenote: this blog uses the <a href="https://nhoizey.github.io/jekyll-cloudinary/">jekyll-cloudinary</a> plugin to automatically process images</li>
</ul></li>
<li>Use &#x201C;just-in-time&#x201D; preloading (highly recommended for improved UX)
<ul>
<li><a href="https://instant.page/">Instant Page</a></li>
</ul></li>
<li>Avoid using heavy tech-stacks whenever possible
<ul>
<li>Unless it is a critical use-case, users should not have to process or download extra resources</li>
<li>This also includes remove ads, pop-ups, 3rd party sign-up prompts, cookie notifications, over-the-top element animations, and all other <strong>garbage</strong>. This impacts <em>UX</em> performance, which is just as crucial as website loading speed</li>
</ul></li>
</ol>
<h2 id="no-need-to-be-extreme">No need to be extreme</h2>
<p>These quick &#x201C;guidelines&#x201D; are just a solid jumping-off point when tackling new projects or re-working current websites. There isn&#x2019;t some agreed upon <em>golden standard</em> when it comes to web performance, but I find these rules work as a great place to start. Hopefully it can help others as well.</p>
<div class="tip">
    <h3>Found this article helpful?</h3>
    <p>Please consider donating to help pay for hosting costs and support the creation of new articles, demos, tutorials, etc. I greatly appreciate it.</p>
    <a href="/donate.html" class="button">Support</a>
</div>
</article>
<footer>
    <p>Powered by <a target="_blank" href="https://pblog.xyz">pblog</a><br>
    Made with &hearts; for a simpler web<br>
    <a href="/donate.html">Support this website</a></p>
</footer>
<script data-goatcounter="https://tdarb.goatcounter.com/count"
        async src="//gc.zgo.at/count.js"></script>
</body>
</html>
