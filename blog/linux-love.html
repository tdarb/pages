<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="dcterms.date" content="2022-07-14" />
  <title>The Linux Desktop is Hard to Love</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="../style.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<nav>
    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about.html">About</a></li>
        <li><a href="/blog">Blog</a></li>
        <li><a href="/uses.html">Uses</a></li>
        <li><a href="/feeds/posts.xml">RSS</a></li>
    </ul>
</nav>
<article>
<header id="title-block-header">
<h1 class="title">The Linux Desktop is Hard to Love</h1>
<p class="date">July 14, 2022</p>
</header>
<nav id="TOC" role="doc-toc">
<ul>
<li><a href="#what-the-linux-desktop-gets-right" id="toc-what-the-linux-desktop-gets-right">What the Linux Desktop Gets Right</a></li>
<li><a href="#what-the-linux-desktop-gets-wrong" id="toc-what-the-linux-desktop-gets-wrong">What the Linux Desktop Gets Wrong</a></li>
<li><a href="#user-experience" id="toc-user-experience">User Experience</a></li>
<li><a href="#quality-hardware" id="toc-quality-hardware">Quality Hardware</a></li>
<li><a href="#i-still-use-linux" id="toc-i-still-use-linux">I Still Use Linux</a></li>
</ul>
</nav>
<p>I want to love the &#x201C;Linux Desktop&#x201D;. I really do. But I&#x2019;ve come to the realization that what I love is the <em>idea</em> of the Linux Desktop. The community. The security and core focus on open source. The customizable environments. Tweaking as much or as little of the operating system as I please!</p>
<p>I just can&#x2019;t <em>stick with it</em>. I always end up back on macOS. And I&#x2019;m starting to understand why.</p>
<h3 id="what-the-linux-desktop-gets-right">What the Linux Desktop Gets Right</h3>
<p>To be fair, there is an incredible amount of things that the Linux desktop does really well:</p>
<ul>
<li>Complete user control</li>
<li>Ability to drastically change the desktop UI
<ul>
<li>Gnome, KDE, XFCE, etc.</li>
</ul></li>
<li>Overall good and welcoming communities</li>
<li>Extensive documentation for almost everything</li>
</ul>
<p>These things make Linux a solid experience overall - but not a <em>great</em> one&#x2026;</p>
<h3 id="what-the-linux-desktop-gets-wrong">What the Linux Desktop Gets Wrong</h3>
<p>If I had to summarize in a word what Linux lacks compared to macOS it would be: <em>cohesion</em>.</p>
<p>Apple&#x2019;s macOS keeps a solid consistency throughout its entire design. Everything looks and feels like it is part of the same system. Which is what a fully-fledged OS <em>should</em> feel like. The argument can be made that macOS suffers some fragmentation with things like <code>homebrew</code>, applications directly from developers vs.&#xA0;applications via the Mac App Store.</p>
<p>While this is true, I believe Linux desktops suffer far worse in terms of fragmented systems. Users building applications from source, <code>snap</code> packages, <code>flathub</code> packages, custom package managers shipped with separate distros, etc. And with this fragmentation comes the constant debates and discussions around which to use and which to avoid.</p>
<p>This can become overwhelming for average computer users. This is something we tend to forget in our &#x201C;tech hubs&#x201D;. Most users want to boot up their machine and get to work. Linux can absolutely do this, but if a user hits a minor snag, then I guarantee they will have more difficulty fixing it compared to an issue found in macOS.</p>
<h3 id="user-experience">User Experience</h3>
<p>Design is important. The user experience will make or break an operating system. This is another issue I&#x2019;ve found with many Linux desktops.</p>
<p>Let&#x2019;s take Bluetooth for example. It works flawlessly in macOS. I have never had a single device bug-out or refuse to connect. Devices connect almost immediately when pairing. The UI is intuitive and gives the user clear feedback to what the system is doing while pairing, disconnecting, and so on.</p>
<p>Now, compare this to an average Linux DE experience - not so seamless. The fact that some distros require you to hop into a terminal in order to properly configure Bluetooth is pretty terrible. Sure, most have GUIs setup similar to that of macOS, but I find myself time and time again needing to pop open that trusty ol&#x2019; Terminal. This is fine for someone like myself, but for the average computer user? No way.</p>
<p>Looking for another example? Printers. Yes, printers are terrible machines created in the depths of Hell itself, but they are a necessary evil. And again, macOS handles &#x201C;plug-and-play&#x201D; printer functionality like a champ. Linux on the other hand is a mixed bag. I&#x2019;ve had some luck with specific Linux distros working with printers in this &#x201C;plug-and-play&#x201D; fashion, while others become a battle of attrition<a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a>. Let&#x2019;s not even begin to talk about wireless <em>only</em> printers and setting up their proper drivers on Linux.</p>
<h3 id="quality-hardware">Quality Hardware</h3>
<p>Another advantage macOS has over most other Linux desktops is tailored hardware. Apple produces the hardware created to run their own operating system, meaning it was specifically built for that task. Linux desktops are designed to run on almost any<a href="#fn2" class="footnote-ref" id="fnref2" role="doc-noteref"><sup>2</sup></a> piece of hardware. Though this is fantastic in terms of technological sustainability (avoids dumping old devices when they lose &#x201C;support&#x201D;) it ends up causing more support issues. Needing to support such a wide range of chip sets and drivers spreads the focus on a streamlined UX a little more thin. It becomes difficult to perfect a cohesive experience user-to-user when some many variables can be different. I should note that some distros<a href="#fn3" class="footnote-ref" id="fnref3" role="doc-noteref"><sup>3</sup></a> are making fantastic strides in this area but are still far from ideal.</p>
<h3 id="i-still-use-linux">I Still Use Linux</h3>
<p>I might have attacked the overall Linux desktop experience in favor of macOS a little harshly in this post, but it&#x2019;s a simple reflection of a individual who has used both extensively. I still work with multiple Linux machines daily. I still <em>like</em> using Linux.</p>
<p>I just don&#x2019;t <em>love</em> it.</p>
<section class="footnotes footnotes-end-of-document" role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>People swear by CUPS working well on Linux, but this has caused issues for me in the past as well. Unsure why macOS handles it fine&#x2026;<a href="#fnref1" class="footnote-back" role="doc-backlink">&#x21A9;&#xFE0E;</a></p></li>
<li id="fn2" role="doc-endnote"><p>Depending on the desired distro, resources required, etc.<a href="#fnref2" class="footnote-back" role="doc-backlink">&#x21A9;&#xFE0E;</a></p></li>
<li id="fn3" role="doc-endnote"><p>A couple that come to mind are Zorin OS and elementary OS<a href="#fnref3" class="footnote-back" role="doc-backlink">&#x21A9;&#xFE0E;</a></p></li>
</ol>
</section>
<div class="tip">
    <h3>Found this article helpful?</h3>
    <p>Please consider donating to help pay for hosting costs and support the creation of new articles, demos, tutorials, etc. I greatly appreciate it.</p>
    <a href="/donate.html" class="button">Support</a>
</div>
</article>
<footer>
    <p>Powered by <a target="_blank" href="https://pblog.xyz">pblog</a><br>
    Made with &hearts; for a simpler web<br>
    <a href="/donate.html">Support this website</a></p>
</footer>
<script data-goatcounter="https://tdarb.goatcounter.com/count"
        async src="//gc.zgo.at/count.js"></script>
</body>
</html>
