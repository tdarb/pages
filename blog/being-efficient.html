<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="dcterms.date" content="2019-08-28" />
  <meta name="description" content="Talking about being more efficient with your time as a designer" />
  <title>Being More Efficient as a Designer and Developer</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <link rel="stylesheet" href="../style.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<nav>
    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about.html">About</a></li>
        <li><a href="/blog">Blog</a></li>
        <li><a href="/uses.html">Uses</a></li>
        <li><a href="/feeds/posts.xml">RSS</a></li>
    </ul>
</nav>
<article>
<header id="title-block-header">
<h1 class="title">Being More Efficient as a Designer and Developer</h1>
<p class="date">August 28, 2019</p>
</header>
<nav id="TOC" role="doc-toc">
<ul>
<li><a href="#before-we-begin" id="toc-before-we-begin">Before we begin&#x2026;</a></li>
<li><a href="#some-vague-examples" id="toc-some-vague-examples">Some vague examples</a></li>
<li><a href="#but-wait-how-when-do-i-learn-new-things" id="toc-but-wait-how-when-do-i-learn-new-things">But wait, how / when do I learn new things?</a></li>
<li><a href="#easier-said-than-done" id="toc-easier-said-than-done">Easier said than done</a></li>
</ul>
</nav>
<p><em>I recently began working on a small side project</em> (a marketing site / blog for an upcoming UX book I&#x2019;m writing, but I have nothing to promote yet - sorry) and found myself circling around different static site generators (SSG) in the initial design concepts. The thought of learning an entirely new blogging platform was inspiring and seemed like a good excuse to expand my skillset.</p>
<p>Although I&#x2019;ve used 11ty and Hugo in the past for client work, this personal website runs on Jekyll. I&#x2019;m very familiar with Jekyll and can push out a point-of-concept site in a flash with little-to-no effort. So, why was I looking to jump into a SSG I hadn&#x2019;t used before?</p>
<p>And that got me thinking&#x2026; <strong>Why am I moving away from being efficient?</strong></p>
<h2 id="before-we-begin">Before we begin&#x2026;</h2>
<p>I should preface everything else I&#x2019;m going to mention in this post with this: <em>learning new stuff is awesome</em>. You should expand your knowledge as much as you can, no matter what industry you find yourself in. I&#x2019;ve found it to be a great catalyst for boosting my passion in design and development.</p>
<p>Okay, I&#x2019;ve made it clear that learning is important to the growth of your career - so please keep that in mind before you read my next statement:</p>
<p><strong>Just use what you already know.</strong></p>
<p>By using your current experience (maybe even expertise) with a design system, CSS framework, blogging platform, programming language, etc. you can get something <em>built</em>. Not to mention you can get that thing built in a <em>fraction of the time</em>. After all, building things is kind of the point of being a designer (or developer), right?</p>
<p>My current side project may be a slight edge case in this regard. Since it&#x2019;s a personal &#x201C;dev&#x201D; website, most of the tech stack choices comes down to personal preference - not client requirements. But I believe my point still remains: you shouldn&#x2019;t reach for something new and shiny <em>just because</em> it&#x2019;s new and shiny.</p>
<h2 id="some-vague-examples">Some vague examples</h2>
<p>It might be easier to understand what I mean by using some possible real-world examples:</p>
<table>
<colgroup>
<col style="width: 37%" />
<col style="width: 27%" />
<col style="width: 35%" />
</colgroup>
<thead>
<tr class="header">
<th>Problem</th>
<th>New Way</th>
<th>Efficient Way</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>A local bakery needs product and e-cart functionality</td>
<td>Learn a new custom ecommerce platform</td>
<td>Use a popular pre-existing library you&#x2019;re familiar with</td>
</tr>
<tr class="even">
<td>Create an add-on blog for a medical clinic</td>
<td>Try a custom built static site generator and hook in a git-based CMS</td>
<td>Spin up a quick WordPress site and hand-off</td>
</tr>
<tr class="odd">
<td>UI mockups for a workout iOS app</td>
<td>Test out the newest design tool just released</td>
<td>Use your go-to default design tool you (Sketch, Figma, etc)</td>
</tr>
</tbody>
</table>
<p>I know all of this is very much &#x201C;common sense&#x201D;, but you would be surprised how often we reach out for the latest and greatest tools (we are creative problem-solvers, after-all). If a current project allots you the time to learn a new skillset alongside outputting a quality product - then more power to you. In my experience that&#x2019;s a rare luxury, so my advice is to focus on shipping quality work (whether that&#x2019;s code, design, analytics, content, etc) instead of getting caught up in the &#x201C;new and shiny&#x201D;.</p>
<h2 id="but-wait-how-when-do-i-learn-new-things">But wait, how / when do I learn new things?</h2>
<p>It isn&#x2019;t exactly ground breaking to state that you should keep things simple as a developer. There are probably hundreds of posts on the web advocating for the exact same thing - which is good. At the same time, we as designers and developers need to avoid stagnation - something that can happen all too easily.</p>
<p>So how do we learn new things? This is a hard thing to answer. Really, the best response would be: <strong>it depends on the designer / developer</strong>. I know, <em>what a cop-out</em>. Unfortunately, it&#x2019;s true. There is no one solution to learning anything new.</p>
<p>The best I can do is offer up some possible options:</p>
<ul>
<li>
Learn outside of work
<ul>
<li>
Reading / listening to a technical book on your commute or before bed
</li>
<li>
Take an online course you can work on after hours
</li>
</ul>
<pre><code>&lt;/li&gt;</code></pre>
</ul>
<ul>
<li>
Contribute to an open source project that you aren&#x2019;t familiar with but are interested in
<ul>
<li>
Even tiny contributions go a long way, don&#x2019;t doubt yourself so much
</li>
</ul>
<pre><code>&lt;/li&gt;</code></pre>
</ul>
<ul>
<li>
Ask your current company (if not a freelancer that is) to learn on their time
<ul>
<li>
It&#x2019;s a valid argument that your company should have vested interest in you becoming a better developer / designer
</li>
</ul>
<pre><code>&lt;/li&gt;</code></pre>
</ul>
<h2 id="easier-said-than-done">Easier said than done</h2>
<p>Sometimes, even the suggestions above don&#x2019;t work for certain individuals. Life is hectic and other important things can pop-up taking precedence. Don&#x2019;t let it get you down - there are more important things in life than mastering the newest framework that released 25 minutes ago.</p>
<p>My motto is to keep shipping quality products that you actually give a shit about. Otherwise it doesn&#x2019;t matter how &#x201C;new&#x201D; it is.</p>
<div class="tip">
    <h3>Found this article helpful?</h3>
    <p>Please consider donating to help pay for hosting costs and support the creation of new articles, demos, tutorials, etc. I greatly appreciate it.</p>
    <a href="/donate.html" class="button">Support</a>
</div>
</article>
<footer>
    <p>Powered by <a target="_blank" href="https://pblog.xyz">pblog</a><br>
    Made with &hearts; for a simpler web<br>
    <a href="/donate.html">Support this website</a></p>
</footer>
<script data-goatcounter="https://tdarb.goatcounter.com/count"
        async src="//gc.zgo.at/count.js"></script>
</body>
</html>
